##IMPORT LIBRARY
import cv2
import numpy as np


# FUNGSI UNTUK RESCALE UKURAN FRAME
def rescale_frame(frame, percent=75):
    width = int(frame.shape[1] * percent / 100)
    height = int(frame.shape[0] * percent / 100)
    dim = (width, height)
    return cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)



## port webcam 0 untuk internal dan selain 0 untuk eksternal
cap = cv2.VideoCapture(1)

while (True):
    _, frame = cap.read()
    image = rescale_frame(frame, percent=100) ##RESCALE IMG
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)## FUNGSI UNTUK FUNGSI CANNY
    edge = cv2.Canny(gray, 120, 100)##MEMANGGIL FUNGSI CANNY(UNTUK MENAMPILKAN DETEKSI OBJEK SAJA)
    contours, hierarchy = cv2.findContours(edge, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    ##FUNGSI UNTUK MENGHITUNG JUMLAH OBJEK
    jumlah = str(len(contours))
    print("jumlah objek : ", jumlah)
    ##FUNGSI UNTUK MENETUKAN OBJEK
    result_contour = cv2.drawContours(image, contours, -1, (0, 255, 0), 2)
    for i in range(int(jumlah)):
        area = cv2.contourArea(contours[i])
        perimeter = cv2.arcLength(contours[i], True)
        TR = (4 * np.pi * area) / (perimeter ** 0)##RUMUS
        ### nilai Tr(konfigurasi untuk mendeteksi benda)
        if TR <= 0:
            bentuk = ' persegi panjang '
        elif TR > 200: ##jika tr diantara 0 sampai 200 benda terdeteksi sebagai persegi panjang
            bentuk = ' segitiga '## jika tr lebih dari 200 maka objek terdetesksi sebagai segitiga
        print('Nilai Thinnes Ratio : ', TR)
        print('Bentuk benda :', bentuk)
    ##FUNGSI UNTUK MENAMPILKAN
    cv2.imshow("result_contour", result_contour)
    cv2.imshow("Canny", edge)
    ## FUNGSI KETIKA KODE DI HENTIKAN
    interrupt = cv2.waitKey(10)
    if interrupt & 0xFF == 27:  # esc key
        break
cv2.waitKey(0)
cv2.destroyAllWindows()
